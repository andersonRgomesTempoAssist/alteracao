from kink import inject

from data.models import InputLogModel
from data.usecase import LogEntradaUseCase


@inject()
class SaveLogInputUseCase:

    def __init__(self, log_input_use_case: LogEntradaUseCase):
        self.data_use_case = log_input_use_case

    def find_by_id(self, id_seq_log):
        return self.data_use_case.find_by_id(id_seq_log)

    def find(self, id_clientecorporativo, id_contrato, id_tipocarteira, id_estrutura, arquivoorigem):
        return self.data_use_case.find_file(id_clientecorporativo, id_contrato, id_tipocarteira, id_estrutura,
                                            arquivoorigem)

    def save(self, input_model: InputLogModel):
        model = InputLogModel(
            id_seqlog=input_model.id_seqlog,
            id_clientecorporativo=input_model.id_clientecorporativo,
            id_contrato=input_model.id_contrato,
            data=input_model.data,
            id_tipocarteira=input_model.id_tipocarteira,
            hora=input_model.hora,
            arquivoorigem=input_model.arquivoorigem,
            totalregistros=input_model.totalregistros,
            regcorretos=input_model.regcorretos,
            regincorretos=input_model.regincorretos,
            regenvinclusao=input_model.regenvinclusao,
            regenvalteracao=input_model.regenvalteracao,
            regenvcancelamento=input_model.regenvcancelamento,
            regenvreativacao=input_model.regenvreativacao,
            regenvprecadastro=input_model.regenvprecadastro,
            regenverro=input_model.regenverro,
            regenvfatal1=input_model.regenvfatal1,
            regenvfatal2=input_model.regenvfatal2,
            regenvvencido=input_model.regenvvencido,
            regenvlote=input_model.regenvlote,
            id_estrutura=input_model.id_estrutura,
            regjacadastrado=input_model.regjacadastrado,
            regnaoprocessado=input_model.regnaoprocessado,
            regignorado=input_model.regignorado,
            regduplicadocarga=input_model.regduplicadocarga,
            data_fim=input_model.data_fim,
            hora_fim=input_model.hora_fim,
            status=input_model.status
        )

        return self.data_use_case.save(model)
