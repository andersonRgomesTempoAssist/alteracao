import pandas as pd
from kink import inject

from data.usecase.property_covered_item_data_use_case import PropertyCoveredItemUseCase
from infra.entities import PropertyCoveredItemEntity
from tools import get_description_field, get_description_field_int


@inject
class BuildPropertyCoveredItemUseCase:
    def __init__(self, property_covered_item_use_case: PropertyCoveredItemUseCase):
        self.property_covered_item_use_case = property_covered_item_use_case

    def build(self, data_frame_merge, fields_structure, id_client) -> list:
        entities = []
        field_structure = fields_structure.query('tabelaDestino == "PROPRITEMCOBERTO"')
        for index, row in data_frame_merge.iterrows():
            logradouro_end_propr = None
            if 'LOGRADOUROENDPROPR' in field_structure['campoDestino'].values and not pd.isnull(
                    row['LOGRADOUROENDPROPR']):
                logradouro_end_propr = row['LOGRADOUROENDPROPR']
                logradouro_end_propr = logradouro_end_propr[0:PropertyCoveredItemEntity.logradouroendpropr.type.length]

            pais_end_propr = get_description_field(field_structure, 'PAISENDPROPR', row)
            cep_end_propr = get_description_field(field_structure, 'CEPENDPROPR', row)
            bairro_end_propr = get_description_field(field_structure, 'BAIRROENDPROPR', row)
            complento_end_propr = get_description_field(field_structure, 'COMPLEMENTOENDPROPR', row)
            numero_end_propr = get_description_field(field_structure, 'NUMEROENDPROPR', row)
            ddi_fone_propr = get_description_field(field_structure, 'DDIFONEPROPR', row)
            ddd_fone_propr = get_description_field(field_structure, 'DDDFONEPROPR', row)
            fone_propr = get_description_field(field_structure, 'FONEPROPR', row)
            cidade_end_propr = get_description_field(field_structure, 'CIDADEENDPROPR', row)
            uf_end_pror = get_description_field(field_structure, 'UFENDPROPR', row)
            nome_proprietario = get_description_field(field_structure, 'NOMEPROPRIETARIO', row)
            cnpj_cpf_propr = get_description_field(field_structure, 'CNPJCPFPROPR', row)
            cnpjcpfproprpesquisa = get_description_field_int(field_structure, 'CNPJCPFPROPR', row)
            entities.append(PropertyCoveredItemEntity(id_propritemcoberto=row['id_itemcoberto'],
                                                      nomeproprietario=nome_proprietario,
                                                      paisendpropr=pais_end_propr,
                                                      nomeproprietariosoundex=get_description_field(field_structure,
                                                                                                    'NOMEPROPRIETARIOSOUNDEX',
                                                                                                    row),
                                                      cnpjcpfpropr=cnpj_cpf_propr,
                                                      ufendpropr=uf_end_pror,
                                                      cidadeendpropr=cidade_end_propr,
                                                      bairroendpropr=bairro_end_propr,
                                                      cependpropr=cep_end_propr,
                                                      logradouroendpropr=logradouro_end_propr,
                                                      numeroendpropr=numero_end_propr,
                                                      complementoendpropr=complento_end_propr,
                                                      ddifonepropr=ddi_fone_propr,
                                                      dddfonepropr=ddd_fone_propr,
                                                      fonepropr=fone_propr,
                                                      id_clientecorporativo=id_client,
                                                      cnpjcpfproprpesquisa=cnpjcpfproprpesquisa))
            return entities

    def execute_insert(self, entities):
        return self.property_covered_item_use_case.insert(entities)

    def execute_update(self, entities):
        return self.property_covered_item_use_case.update(entities)
