import abc
from abc import abstractmethod


class IValidation(abc.ABC):

    @abstractmethod
    def build_validation(self):
        pass
