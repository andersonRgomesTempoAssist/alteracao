from kink import inject

from data.helpers import ExceptionData
from infra.repositories import CoveredItemRepository


@inject()
class CoveredItemDataUseCase:
    def __init__(self, repository: CoveredItemRepository):
        self.repository = repository

    def find_covered_item(self, id_client, id_contract, array_policy, array_policy_item,
                          array_car_number, array_id_plano):
        if id_client is None:
            raise ExceptionData(0, 'id_client is necessary')

        if array_policy is None:
            raise ExceptionData(0, 'array_policy is necessary')

        if array_policy_item is None:
            raise ExceptionData(0, 'array_policy_item is necessary')

        if array_car_number is None:
            raise ExceptionData(0, 'array_car_number is necessary')

        if id_contract is None:
            raise ExceptionData(0, 'id_contract is necessary')
        try:
            record_native = self.repository.find_covered_item(id_client=id_client, id_contract=id_contract,
                                                              array_policy=array_policy,
                                                              array_policy_item=array_policy_item,
                                                              array_car_number=array_car_number,
                                                              array_id_plano=array_id_plano)

            return record_native
        except Exception:
            raise

    def insert_update_covered_item(self, entities):
        if entities is None or len(entities) == 0:
            raise ExceptionData(0, 'entities is necessary')
        try:
            record = self.repository.insert_covered_item(entities)
            return record
        except Exception:
            raise

    def update_covered_item(self, entities):
        if entities is None or len(entities) == 0:
            raise ExceptionData(0, 'entities is necessary')
        try:
            record = self.repository.update_covered_item(entities)
            return record
        except Exception:
            raise
