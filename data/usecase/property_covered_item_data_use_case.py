from kink import inject

from infra.repositories import PropertyCoveredItemRepository


@inject()
class PropertyCoveredItemUseCase:
    def __init__(self, repository: PropertyCoveredItemRepository):
        self.repository = repository

    def insert(self, entities):
        if len(entities) == 0:
            raise
        return self.repository.insert(entities)

    def update(self, entities):
        if len(entities) == 0:
            raise
        return self.repository.update(entities)