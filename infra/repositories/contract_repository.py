import logging
from collections import namedtuple

import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound

from infra.config import DBConnectionHandler
from infra.entities import ContractEntity
from infra.helpers import ExceptionRepository


LOGGER = logging.getLogger(__name__)


class ContractRepository:

    def find_by_id(self, id_contract):
        LOGGER.debug("find by contract id, contract %s ", id_contract)
        for i in DBConnectionHandler.__session__():
            try:
                result = i.query(ContractEntity).filter(
                    ContractEntity.id_contrato == id_contract).one()
                Contrato = namedtuple(
                    "Contrato",
                    [
                        "id_contrato",
                        "id_status",
                        "id_clientecorporativo",
                        "numero",
                        "titulo",
                        "datainicio",
                        "datavalidade",
                        "datacadastro"
                    ])
                return Contrato(id_contrato=result.id_contrato, id_status=result.id_status,
                                id_clientecorporativo=result.id_clientecorporativo,
                                numero=result.id_clientecorporativo,
                                titulo=result.titulo, datainicio=result.datainicio,
                                datavalidade=result.datavalidade, datacadastro=result.datacadastro)
            except sqlalchemy.exc.OperationalError as error:
                LOGGER.error("Error to find contract, error %s ", error)
                raise ExceptionRepository('000', 'Internal server Error')
            except NoResultFound:
                LOGGER.error("Contract not found")
                raise ExceptionRepository('000', 'Contract not found')
            except Exception as error:
                LOGGER.error("Internal error, error %s ", error)
                raise ExceptionRepository('000', 'Internal server Error')
