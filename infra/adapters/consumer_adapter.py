# -*- coding: utf-8 -*-
# pylint: disable=C0111,C0103,R0205
import configparser
import functools
import threading
import pika

from tools import get_resource_ini


class ConsumerAdapter(object):

    def on_message(self, ch, method_frame, _header_frame, body, args):
        (conn, thrds, callback_consumers) = args
        delivery_tag = method_frame.delivery_tag
        t = threading.Thread(target=callback_consumers, args=(conn, ch, delivery_tag, body))
        t.start()
        thrds.append(t)

    def __init__(self, callback_consumers):

        config = configparser.ConfigParser()
        config.read(get_resource_ini())
        host_ = config["queue"]["HOST"]
        user = config["queue"]["USER"]
        password = config["queue"]["PASSWORD"]
        port_ = int(config["queue"]["PORT"])
        credentials = pika.PlainCredentials(user, password)

        parameters = pika.ConnectionParameters(
            host=host_, port=port_, credentials=credentials, heartbeat=0)
        connection = pika.BlockingConnection(parameters)

        self.channel = connection.channel()
        threads = []
        on_message_callback = functools.partial(self.on_message, args=(connection, threads, callback_consumers))
        self.channel.basic_consume('process.update', on_message_callback)

        try:
            self.channel.start_consuming()
        except KeyboardInterrupt:
            self.channel.stop_consuming()

        # Wait for all to complete
        for thread in threads:
            thread.join()

        connection.close()
